package code;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.Scanner;

public class $const {
    public static final String FreeLiability="1. 本工具仅起到辅助作用，因本工具造成的财产等损失，本工具作者概不负责！\n2. 在使用特定功能拥有特定的免责声明，如不能同意因停止使用该功能!\n3. 本工具由 Kyuharu 使用Java17编写，部分源码收集于互联网，如有侵权请联系邮箱 kyuharu@foxmail.com";
    public static final String UserMeow="请选择(输入序号): \33[32;4m";
    public static Scanner scanner=new Scanner(System.in);
    public static final String UserMeow_ERR="请输入正确序号哦, 轻按Enter酱返回...";
    public static final String adb_exe=".\\assets\\aft\\adb.exe";
    public static final String fastboot_exe=".\\assets\\aft\\fastboot.exe";
    public static final String tar_exe=".\\assets\\tar.exe";
    public static final String curl_exe=".\\assets\\curl.exe";
    // public static final String zg_adb_exe=".\\assets\\aft\\zg\\adb.exe";
    public static final String zg_fastboot_exe=".\\assets\\aft\\zg\\fastboot.exe";
    public static final String payload_exe=".\\assets\\payload\\payload.exe";
    public static final String aria2c_exe=".\\assets\\aria2c.exe";
    public static final String a7z_exe=".\\assets\\7z\\7za.exe";
    public static final String scrcpy_exe=".\\assets\\scrcpy\\scrcpy.exe";
    public static final String py_exe=".\\plugin\\python\\python.exe";
    public static final String mtk=".\\plugin\\mtkclient\\mtk";
    public static final String py_exe_mtk=$const.py_exe+" "+$const.mtk;
    public static final String pan123=".\\assets\\pan123\\android.exe";
    public static void cmd(String cmd) throws InterruptedException, IOException{
        new ProcessBuilder("cmd", "/c", cmd).inheritIO().start().waitFor();
    }
    public static final String Enter=", 轻按 Enter 返回...";
    public static final String cEnter=", 轻按 Enter 继续...";
    public static final String adb_devices_start="List of devices attached";
    public static String cmd_out(String cmd){
        StringBuilder sBuilder=new StringBuilder();
        try {
                BufferedReader bReader=new BufferedReader(
                    new InputStreamReader(
                        Runtime.getRuntime().exec(cmd).getInputStream()
                    )
                );
                String line;
                while ((line = bReader.readLine()) != null) {
                    sBuilder.append(line).append("\n");
                }
        }catch (Exception ignored) {
        }
        return sBuilder.toString();
    }
    public static String plugin_list_url = "https://fricktools.fun/api/plugin.json";
    public static void DownloadFile(String url, String out) throws IOException, InterruptedException {
        cmd(aria2c_exe+" -d "+ out + " " + url);
    }
    public static String unzip(String in, String out) throws IOException, InterruptedException {
        String WillCMD=a7z_exe+" x "+ " -o" + out +" "+  in;
        String ZipPath = null;
        try {
            BufferedReader bReader=new BufferedReader(
                    new InputStreamReader(
                            Runtime.getRuntime().exec(WillCMD).getInputStream()
                    )
            );
            String line;
            while ((line = bReader.readLine()) != null) {
                if(line.contains("Path")){
                    ZipPath=line.split("=")[1];
                    ZipPath=ZipPath.substring(1);
                }
            }
        }catch (Exception ignored) {
        }
        return ZipPath;
    }
}
