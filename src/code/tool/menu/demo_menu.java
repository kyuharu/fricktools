
// 如你所见，这是一个实例，他将教会你如何使用 code.tool.menu.menu 类

package code.tool.menu;

import java.io.IOException;

import code.menu.home.homeMenu;
import code.$const;
import code.tool.color;

public class demo_menu {
    public static void start() throws InterruptedException, IOException{
        while (true) {
            menu sub=new menu();
            sub.setMenu_name("title");
            sub.setTips("输入 999 返回");
            String[] home_sele = {"one"};
            sub.add_all(home_sele);
            sub.print();
            color.blue_ln($const.UserMeow);
            String UserMeow=$const.scanner.nextLine();
            if (UserMeow.equals("999")) {
                homeMenu.start();
                return;
            }
            if (!menu.CheckMeow(UserMeow, home_sele.length+1, 1)) {
                color.red_bold($const.UserMeow_ERR);
                code.$const.scanner.nextLine();
                start();
                return;
            }
            sele(UserMeow);
        }
    }
    
    private static void sele(String UserMeow) {
        int $UserMeow = Integer.parseInt(UserMeow);
        switch ($UserMeow) {
            case 1:
                // one
                break;
            case 2:
                //
                break;
            default:
                color.red("ERR: 未知错误");
                break;
        }
    }
}