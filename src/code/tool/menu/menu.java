package code.tool.menu;

import code.tool.color;

import java.io.IOException;
import java.util.HashMap;
import java.util.Map;
import java.util.Objects;
import java.util.regex.Pattern;

public class menu {
    String menu_name;
    String tips;
    Map<String, option> menu_sele = new HashMap<>();
    
    public void setMenu_name(String menu_name) {
        this.menu_name = menu_name;
    }

    public void setTips(String tips) {
        this.tips = tips;
    }

    public void add_sele(int num,String name,String color){
        option option = new option();
        option.setName(name);
        option.setColor(Objects.requireNonNullElse(color, "green"));
        this.menu_sele.put(Integer.toString(num), option);
    }

    public void add_all(String[] s){
        for (int i = 0; i < s.length; i++) {
            this.add_sele(i+1, s[i], null);
        }
    }

    public void print() throws InterruptedException, IOException{
        new ProcessBuilder("cmd", "/c", "cls").inheritIO().start().waitFor();
        color.green_bold(this.menu_name);
        color.red("------------");
        if (this.tips != null) {
            color.green("Tips: "+this.tips);
            color.red("------------");
        }
        for (int i = 1; i-1 < this.menu_sele.size(); i++) {
            String name = this.menu_sele.get(Integer.toString(i)).name;
            if (this.menu_sele.get(Integer.toString(i)).color.equals("red")) {
                System.out.println("\33[31;2m(\33[32;1m"+ i +"\33[31;2m) "+name);
            }else{
                System.out.println("\33[32;2m(\33[31;1m"+ i +"\33[32;2m) "+name);
            }
            color.red("------------");
        }
    }

    public static boolean CheckMeow(String Meow, int max,int min){
        // 检测用户有没有瞎喵
        if (Meow == null) {
            return false;
        }
        final Pattern NUMBER_PATTERN = Pattern.compile("-?\\d+(\\.\\d+)?");
        return NUMBER_PATTERN.matcher(Meow).matches() && Integer.parseInt(Meow) <= max - 1 && Integer.parseInt(Meow) >= min;
    }

}

/**
 * option
 */
class option {
    String name;
    String color;

    public void setColor(String color) {
        this.color = color;
    }

    public void setName(String name) {
        this.name = name;
    }

}