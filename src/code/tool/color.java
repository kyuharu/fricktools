package code.tool;

public class color {
    static String osVersion = System.getProperty("os.version");

    public static void green(String x){
        if (osVersion.startsWith("6.1")){
            System.out.println(x);
            return;
        }
        System.out.println("\33[32;2m"+x+"\33[0m");
    }

    public static void green_ln(String x){
        if (osVersion.startsWith("6.1")){
            System.out.println(x);
            return;
        }
        System.out.print("\33[32;2m"+x+"\33[0m");
    }

    public static void green_bold(String x){
        if (osVersion.startsWith("6.1")){
            System.out.println(x);
            return;
        }
        System.out.println("\33[32;1m"+x+"\33[0m");
    }

    public static void red(String x){
        if (osVersion.startsWith("6.1")){
            System.out.println(x);
            return;
        }
        System.out.println("\33[31;2m"+x+"\33[0m");
    }

    public static void red_ln(String x){
        if (osVersion.startsWith("6.1")){
            System.out.println(x);
            return;
        }
        System.out.print("\33[31;2m"+x+"\33[0m");
    }

    public static void red_bold(String x){
        if (osVersion.startsWith("6.1")){
            System.out.println(x);
            return;
        }
        System.out.println("\33[31;1m"+x+"\33[0m");
    }

    public static void blue_ln(String x){
        if (osVersion.startsWith("6.1")){
            System.out.println(x);
            return;
        }
        System.out.print("\33[36;2m"+x+"\33[0m");
    }

    public static void blue(String x){
        if (osVersion.startsWith("6.1")){
            System.out.println(x);
            return;
        }
        System.out.println("\33[36;2m"+x+"\33[0m");
    }
}
