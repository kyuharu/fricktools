package code.tool;

import code.$const;

public class httpGetRequest {
    public static String sendGetRequest(String urlString) {
        return $const.cmd_out($const.curl_exe+" --cacert .\\assets\\curl.pem "+urlString);
    }
}
