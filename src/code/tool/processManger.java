package code.tool;

import java.awt.*;
import java.io.*;
import java.util.ArrayList;
import java.util.List;

public class processManger {
    //得到所有运行的进程名称
    public static String[] getProcessName() {
        String[] taskName = null;
        List<String> taskList = new ArrayList<>();
        try {
            Process p = Runtime.getRuntime().exec("tasklist");
            BufferedReader br = new BufferedReader(new InputStreamReader(p.getInputStream(), "gbk"));
            String s;
            while ((s = br.readLine()) != null) {
                if (s.isEmpty()) {
                    continue;
                }
                taskList.add(s);
            }
            //获取每列的最长长度
            String maxRow = taskList.get(1);
            String[] maxCol = maxRow.split(" ");
            //进程名称
            taskName = new String[taskList.size()];
            for (int i = 0; i < taskList.size(); i++) {
                String data = taskList.get(i);
                for (int j = 0; j < maxCol.length; j++) {
                    if (j == 0) {
                        taskName[i] = data.substring(0, maxCol[j].length() + 1).trim();
                        data = data.substring(maxCol[j].length() + 1);
                    }
                }
            }
        } catch (java.io.IOException e) {
            e.printStackTrace();
        }
        return taskName;
    }

    //查询进程是否开启
    public static String getProcessStatus(String[] taskName, String processName) {
        String s = "此进程已结束";
        for (String string : taskName) {
//            System.out.println(taskName[i]);
            if (string.contains(processName)) {
                s = "此进程已开启";
                break;
            }
        }
        return s;
    }

    //关闭进程
    public static String killProcess(String processName) throws IOException {
        String s = "进程名称不能为空";
        String active = getProcessStatus(getProcessName(), processName);
        if (!"".equals(processName)) {
            if (active.equals("此进程已结束")) {
                s = "进程已关闭，无需再次关闭";
            } else {
                Process p = Runtime.getRuntime().exec("taskkill /F /IM " + processName);
                BufferedReader br = new BufferedReader(new InputStreamReader(p.getInputStream(), "gbk"));
                s = br.readLine();
            }
        }
        return s;
    }

    //开启进程（相当于重启） 若存在该进程，再次开启则存在两个进程
    public static String startProcess(String processName, String url) throws IOException {
        String s = "进程名称不能为空";
        //需要先检测进程是否存在
        getProcessName();
        if (!"".equals(processName)) {
            killProcess(processName);
            Desktop.getDesktop().open(new File(url));
            s = "进程重启成功";
        }
        return s;
    }
    public static void test() throws IOException{
        String[] taskName = getProcessName();
        String s = getProcessStatus(taskName,"chrome.exe");
        System.out.println(s);
        String kill = killProcess("chrome.exe");
        System.out.println(kill);
        String active = startProcess("chrome.exe","C:\\Users\\11654\\AppData\\Local\\Google\\Chrome\\Application\\chrome.exe");
        System.out.println(active);
    }
}