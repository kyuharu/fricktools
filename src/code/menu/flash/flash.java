package code.menu.flash;

import code.$const;
import code.tool.color;

import java.io.File;
import java.io.IOException;

/**
 * flash
 */
class flash {
    public static void all(String subarea, String path) throws InterruptedException, IOException {
        $const.cmd($const.fastboot_exe + " flash " + subarea + " " + path);
    }

    public static void all_auto(String where) throws InterruptedException, IOException {
        String what;
        $const.cmd("cls");
        color.green("请自行将设备重启至 FastBoot 按 Enter 继续...");
        $const.scanner.nextLine();
        switch (where) {
            case "3" -> {
                color.blue_ln("请输入要刷入的分区(如: data、system、boot等): ");
                what = $const.scanner.nextLine();
            }
            case "2" -> what = "recovery";
            case "1" -> what = "boot";
            default -> what = null;
        }
        color.blue_ln("请输入 " + what + " 镜像文件路径 (如: C:\\" + what + ".img): ");
        String UserMeow = $const.scanner.nextLine();
        File all = new File(UserMeow);
        if (!all.exists()) {
            color.red_ln("提示: 您的镜像文件可能不存在, 继续执行可能有风险, 是否执行?\n请输入是或否: ");
            String UserMeow1 = $const.scanner.nextLine();
            if (UserMeow1.equals("否")) {
                return;
            } else if (!(UserMeow1.equals("是"))) {
                return;
            }
        }
        flash.all(what, UserMeow);
        color.green("执行完成" + $const.Enter);
        $const.scanner.nextLine();
    }

    public static void bao(String path) throws InterruptedException, IOException {
        $const.cmd("cls");
        color.blue_ln("此功能需要提前安装 ADB 及 FastBoot 环境, 您是否已安装(输入 是 或 否): ");
        String UserMeow = $const.scanner.nextLine();
        if (UserMeow.equals("否")) {
            return;
        } else if (!(UserMeow.equals("是"))) {
            color.red("错误选择" + $const.Enter);
            $const.scanner.nextLine();
            return;
        }
        if (path == null) {
            color.blue_ln("请输入线刷包解压后的目录路径(如: C:\\xianshua): ");
            UserMeow = $const.scanner.nextLine();
            File file = new File(UserMeow);
            if (file.isDirectory()) {
                path = UserMeow;
            } else {
                color.red("ERR: 您输入的路径不是文件夹或不存在" + $const.Enter);
                $const.scanner.nextLine();
                return;
            }
        }
        label:
        while (true) {
            color.blue_ln("(1) 全部清除并刷入\n(2) 保留 UserData 并刷入\n(3) 刷入并上锁\n请选择刷入模式: ");
            String mode = $const.scanner.nextLine();
            switch (mode) {
                case "1":
                    $const.cmd(path + "\\flash_all.bat");
                    color.green("执行完成" + $const.Enter);
                    $const.scanner.nextLine();
                    break label;
                case "2":
                    $const.cmd(path + "\\flash_all_except_storage.bat");
                    color.green("执行完成" + $const.Enter);
                    $const.scanner.nextLine();
                    break label;
                case "3":
                    $const.cmd(path + "\\flash_all_lock.bat");
                    color.green("执行完成" + $const.Enter);
                    $const.scanner.nextLine();
                    break label;
                default:
                    color.red("ERR: 选择错误" + $const.Enter);
                    $const.scanner.nextLine();
                    break;
            }
        }
    }
}
