package code.menu.flash;

import java.io.IOException;

import code.tool.menu.menu;
import code.$const;
import code.tool.color;
import code.menu.home.homeMenu;

public class flashMenu {
    public static void start() throws InterruptedException, IOException{
        while (true) {
            menu sub=new menu();
            sub.setMenu_name("通用刷机菜单");
            sub.setTips("输入 999 返回");
            String[] home_sele = {"刷入BOOT","刷入Recovery","刷入任意分区","一键刷入线刷包"};
            sub.add_all(home_sele);
            sub.print();
            color.blue_ln($const.UserMeow);
            String UserMeow=$const.scanner.nextLine();
            if (UserMeow.equals("999")) {
                homeMenu.start();
                return;
            }
            if (!menu.CheckMeow(UserMeow, home_sele.length+1, 1)) {
                color.red_bold($const.UserMeow_ERR);
                code.$const.scanner.nextLine();
                start();
                return;
            }
            sele(UserMeow);
        }
    }
    
    private static void sele(String UserMeow) throws InterruptedException, IOException{
        int $UserMeow = Integer.parseInt(UserMeow);
        switch ($UserMeow) {
            case 1:
                flash.all_auto("1");
                break;
            case 2:
                flash.all_auto("2");
                break;
            case 3:
                flash.all_auto("3");
                break;
            case 4:
                flash.bao(null);
                break;
            default:
                color.red("ERR: 未知错误");
                break;
        }
    }
}

