
// 如你所见，这是一个实例，他将教会你如何使用 code.tool.menu.menu 类

package code.menu.AdbFb;

import java.io.IOException;
import java.util.List;

import code.tool.menu.menu;
import code.$const;
import code.tool.color;
import code.menu.AdbFb.adb.adb;
import code.menu.AdbFb.adb.adbMenu;
import code.menu.AdbFb.fb.fastboot;
import code.menu.AdbFb.fb.fastbootMenu;
import code.menu.home.homeMenu;

public class AdbFbMenu {
    public static void start() throws InterruptedException, IOException{
        while (true) {
            menu sub=new menu();
            sub.setMenu_name("ADB 及 FastBoot 菜单");
            sub.setTips("输入 999 返回");
            String[] home_sele = {"检测设备","ADB 菜单", "FastBoot 菜单"};
            sub.add_all(home_sele);
            sub.print();
            color.blue_ln($const.UserMeow);
            String UserMeow=$const.scanner.nextLine();
            if (UserMeow.equals("999")) {
                homeMenu.start();
                return;
            }
            if (!menu.CheckMeow(UserMeow, home_sele.length+1, 1)) {
                color.red_bold($const.UserMeow_ERR);
                $const.scanner.nextLine();
                start();
                return;
            }
            sele(UserMeow);
        }
    }

    private static void sele(String UserMeow) throws InterruptedException, IOException{
        int $UserMeow = Integer.parseInt(UserMeow);
        switch ($UserMeow) {
            case 1:
                CheckDevice();
                break;
            case 2:
                adbMenu.start();
                break;
            case 3:
                fastbootMenu.start();
                break;
            default:
                color.red("ERR: 未知错误");
                break;
        }
    }

    public static void CheckDevice() {
        List<device> adb_devices = adb.getDevices();
        List<device> fb_devices = fastboot.getDevices();
        if (adb_devices != null) {
            if (adb_devices.isEmpty() && fb_devices.isEmpty()){
                color.red("没有设备连接"+$const.Enter);
                $const.scanner.nextLine();
                return;
            }
        } else {
            color.red("出现未知错误"+$const.Enter);
            $const.scanner.nextLine();
            return;
        }

        color.green_ln("发现 ");
        color.blue_ln(Integer.toString(adb_devices.size()+fb_devices.size()));
        color.green(" 个设备: ");

        for (int i = 0; i < adb_devices.size(); i++){
            color.green_ln("(");
            color.red_ln(Integer.toString(i+1));
            color.green_ln(") 设备序列号: ");
            color.blue_ln(adb_devices.get(i).getName());
            color.green_ln(" 设备状态: ");
            color.blue(adb_devices.get(i).getStatus());
        }

        for (int i = 0; i < fb_devices.size(); i++){
            color.green_ln("(");
            color.red_ln(Integer.toString(i+1+adb_devices.size()));
            color.green_ln(") 设备序列号: ");
            color.blue_ln(fb_devices.get(i).getName());
            color.green_ln(" 设备状态: ");
            color.blue(fb_devices.get(i).getStatus());
        }

        color.green("执行完成"+$const.Enter);
        $const.scanner.nextLine();
    }
}