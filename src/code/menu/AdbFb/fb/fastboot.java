package code.menu.AdbFb.fb;

import code.$const;
import code.tool.color;
import code.menu.AdbFb.device;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

public class fastboot {
    public static void reboot(String to) throws InterruptedException, IOException {
        if (to == null) {
            $const.cmd($const.fastboot_exe + " reboot");
            return;
        }
        $const.cmd($const.fastboot_exe + " reboot " + to);
    }

    public static void erase(String subarea) throws InterruptedException, IOException {
        $const.cmd("cls");
        if (subarea == null) {
            color.blue_ln("请输入要擦除的分区: ");
            subarea = $const.scanner.nextLine();
        }
        color.red_ln("您正在执行危险操作, 此操作不可逆, 真的要执行吗?\n(1) 是\n(2) 否\n请选择: ");
        String UserMeow = $const.scanner.nextLine();
        if (!(UserMeow.equals("是"))) {
            color.green("已终止此操作" + $const.Enter);
            $const.scanner.nextLine();
            return;
        }
        $const.cmd($const.fastboot_exe + " erase " + subarea);
        color.green("执行完成" + $const.Enter);
        $const.scanner.nextLine();
    }

    public static void bl_status() throws InterruptedException, IOException {
        $const.cmd("cls");
        $const.cmd($const.fastboot_exe + " oem device-info");
        color.green("执行成功" + $const.Enter);
        $const.scanner.nextLine();
    }

    public static void boot(String path) throws IOException, InterruptedException {
        $const.cmd("cls");
        if (path == null) {
            color.blue_ln("请输入BOOT镜像路径: ");
            path = $const.scanner.nextLine();
        }
        $const.cmd($const.fastboot_exe + " boot " + path);
        color.green("执行成功" + $const.Enter);
        $const.scanner.nextLine();
    }
    public static List<device> getDevices() {
        List<device> devices = new ArrayList<>();
        String devices_out=$const.cmd_out($const.fastboot_exe+" devices");
        if (devices_out.isEmpty()){
            return devices;
        }
        String[] Lines = devices_out.split("\n");
        for (String line : Lines) {
            String[] split = line.split("\t");
            if (split.length < 2) {
                continue;
            }
            devices.add(new device(split[0], split[1]));
        }
        return devices;
    }
    public static void FlashAll(String Directory) throws IOException, InterruptedException {
        boolean b = false;
        if (Directory == null) {
            color.blue_ln("请输入要刷入的目录: ");
            Directory = $const.scanner.nextLine();
            b = true;
        }
        if (!(new File(Directory).isDirectory())) {
            color.blue_ln("文件不存在是否继续(输入 是 或 否): ");
            String UserMeow = $const.scanner.nextLine();
            if (UserMeow.equals("否")) {
                color.green("已终止此操作" + $const.Enter);
                $const.scanner.nextLine();
                return;
            } else if (!(UserMeow.equals("是"))) {
                color.red("选择错误" + $const.Enter);
                $const.scanner.nextLine();
                return;
            }
        }
        $const.cmd($const.fastboot_exe + " flashall " + Directory);
        if (b) {
            color.green("执行完成" + $const.Enter);
            $const.scanner.nextLine();
        }
    }
}
