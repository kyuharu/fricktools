package code.menu.AdbFb.fb;

import java.io.IOException;

import code.tool.menu.menu;
import code.$const;
import code.tool.color;
import code.menu.AdbFb.AdbFbMenu;

public class fastbootMenu {
    public static void start() throws InterruptedException, IOException {
        while (true) {
            menu sub = new menu();
            sub.setMenu_name("FastBoot菜单");
            sub.setTips("输入 999 返回");
            String[] home_sele = {
                    "重启至系统",
                    "重启至Recovery",
                    "重启至EDL(9008,仅高通)",
                    "擦除分区",
                    "查看BL锁状态(部分机型不支持)",
                    "临时启动BOOT",
                    "刷入指定文件夹的全部镜像"};
            sub.add_all(home_sele);
            sub.print();
            color.blue_ln($const.UserMeow);
            String UserMeow = $const.scanner.nextLine();
            if (UserMeow.equals("999")) {
                AdbFbMenu.start();
                return;
            }
            if (!menu.CheckMeow(UserMeow, home_sele.length + 1, 1)) {
                color.red_bold($const.UserMeow_ERR);
                code.$const.scanner.nextLine();
                start();
                return;
            }
            sele(UserMeow);
        }
    }

    private static void sele(String UserMeow) throws InterruptedException, IOException {
        int $UserMeow = Integer.parseInt(UserMeow);
        switch ($UserMeow) {
            case 1:
                $const.cmd("cls");
                fastboot.reboot(null);
                color.green("执行成功" + $const.Enter);
                $const.scanner.nextLine();
                break;
            case 2:
                $const.cmd("cls");
                fastboot.reboot("recovery");
                color.green("执行成功" + $const.Enter);
                $const.scanner.nextLine();
                break;
            case 3:
                $const.cmd("cls");
                fastboot.reboot("edl");
                color.green("执行成功" + $const.Enter);
                $const.scanner.nextLine();
                break;
            case 4:
                fastboot.erase(null);
                break;
            case 5:
                fastboot.bl_status();
                break;
            case 6:
                fastboot.boot(null);
                break;
            case 7:
                fastboot.FlashAll(null);
                break;
            default:
                color.red("ERR: 未知错误");
                break;
        }
    }
}

