package code.menu.AdbFb;

public class device {
    String name;
    String status;

    public device(String s, String s1) {
        this.name = s;
        this.status = s1;
    }

    public void setName(String name) {
        this.name = name;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getName() {
        return name;
    }

    public String getStatus() {
        return status;
    }
}
