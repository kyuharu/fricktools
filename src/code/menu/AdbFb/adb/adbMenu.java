package code.menu.AdbFb.adb;

import java.io.IOException;

import code.tool.menu.menu;
import code.$const;
import code.tool.color;
import code.menu.AdbFb.AdbFbMenu;

public class adbMenu {
    public static void start() throws InterruptedException, IOException{
        while (true) {
            menu sub=new menu();
            sub.setMenu_name("ADB菜单");
            sub.setTips("输入 999 返回");
            String[] home_sele = {"重启",
            "重启至Fastboot",
            "重启至Recovery",
            "重启至Download",
            "安装APK",
            "打开Shell终端",
            "截图",
            "无线调试",
            "激活APP菜单(ADB)",
            "修改分辨率",
            "应用管理菜单",
            "SideLoad 旁加载",
            "投屏控制器"};
            sub.add_all(home_sele);
            sub.print();
            color.blue_ln($const.UserMeow);
            String UserMeow=$const.scanner.nextLine();
            if (UserMeow.equals("999")) {
                AdbFbMenu.start();
                return;
            }
            if (!menu.CheckMeow(UserMeow, home_sele.length+1, 1)) {
                color.red_bold($const.UserMeow_ERR);
                code.$const.scanner.nextLine();
                start();
                return;
            }
            sele(UserMeow);
        }
    }
    
    private static void sele(String UserMeow) throws InterruptedException, IOException{
        int $UserMeow = Integer.parseInt(UserMeow);
        switch ($UserMeow) {
            case 1:
                adb.reboot(null);
                color.green("执行成功"+$const.Enter);
                $const.scanner.nextLine();
                break;
            case 2:
                adb.reboot("bootloader");
                color.green("执行成功"+$const.Enter);
                $const.scanner.nextLine();
                break;
            case 3:
                adb.reboot("recovery");
                color.green("执行成功"+$const.Enter);
                $const.scanner.nextLine();
                break;
            case 4:
                adb.reboot("download");
                color.green("执行成功"+$const.Enter);
                $const.scanner.nextLine();
                break;
            case 5:
                adb.install(null);
                break;
            case 6:
                $const.cmd("cls");
                $const.cmd($const.adb_exe+" shell");
                break;
            case 7:
                adb.srccer();
                break;
            case 8:
                adb.connect(null, null);
                break;
            case 9:
                activationAppMenu.start();
                return;
            case 10:
                adb.wm_size("query", null, null,null);
                color.green("(1) 设置分辨率和DPI\n(2) 重置");
                color.blue_ln("请选择: ");
                UserMeow=$const.scanner.nextLine();
                if (UserMeow.equals("1")) {
                    adb.wm_size("set", null, null, null);
                    break;
                }else if (UserMeow.equals("2")) {
                    adb.wm_size("reset", null, null, null);
                    break;
                }else{
                    color.red("请选择指定序号"+$const.Enter);
                    $const.scanner.nextLine();
                    break;
                }
            case 11:
                appMangerMenu.start();
                return;
            case 12:
                adb.sideload(null);
                break;
            case 13:
                adb.scrcpy("Menu");
                break;
            default:
                color.red("ERR: 未知错误");
                break;
        }
    }
}

