package code.menu.AdbFb.adb;

import code.$const;
import code.tool.menu.menu;
import code.tool.color;

import java.io.IOException;

/**
 * appManger
 */
class appManger {
    public static void app_list(String mode) throws InterruptedException, IOException {
        if (mode == null) {
            color.red("ERR: 参数异常" + $const.Enter);
            $const.scanner.nextLine();
            return;
        }
        // Get have
        switch (mode) {
            case "have" -> {
                $const.cmd("cls");
                String packages = $const.cmd_out($const.adb_exe + " shell pm list packages");
                packages = packages.replace("\n", "");
                String[] packagesStrings = packages.split("package:");
                int packageint = 0;
                for (String string : packagesStrings) {
                    if (!(string.isEmpty())) {
                        packageint++;
                        color.green_ln("(" + packageint + ") 应用包名: ");
                        color.blue(string);
                    }
                }
            }

            // Get real
            case "real" -> {
                $const.cmd("cls");
                String packages = $const.cmd_out($const.adb_exe + " shell pm list packages -e");
                packages = packages.replace("\n", "");
                String[] packagesStrings = packages.split("package:");
                int packageint = 0;
                for (String string : packagesStrings) {
                    if (!(string.isEmpty())) {
                        packageint++;
                        color.green_ln("(" + packageint + ") 应用包名: ");
                        color.blue(string);
                    }
                }
            }

            // Get dis
            case "dis" -> {
                $const.cmd("cls");
                String packages = $const.cmd_out($const.adb_exe + " shell pm list packages -d");
                packages = packages.replace("\n", "");
                String[] packagesStrings = packages.split("package:");
                int packageint = 0;
                for (String string : packagesStrings) {
                    if (!(string.isEmpty())) {
                        packageint++;
                        color.green_ln("(" + packageint + ") 应用包名: ");
                        color.blue(string);
                    }
                }
            }
        }
    }

    public static void app_list_ui() throws InterruptedException, IOException {
        menu app_listMenu = new menu();
        app_listMenu.setMenu_name("应用列表");
        String[] applist_Strings = {"当前系统安装的应用",
                "未冻结的应用",
                "已冻结的应用"};
        app_listMenu.add_all(applist_Strings);
        app_listMenu.print();
        color.blue_ln($const.UserMeow);
        String UserMeow = $const.scanner.nextLine();
        switch (UserMeow) {
            case "1" -> {
                app_list("have");
                color.green("执行成功" + $const.Enter);
                $const.scanner.nextLine();
            }
            case "2" -> {
                app_list("real");
                color.green("执行成功" + $const.Enter);
                $const.scanner.nextLine();
            }
            case "3" -> {
                app_list("dis");
                color.green("执行成功" + $const.Enter);
                $const.scanner.nextLine();
            }
            default -> {
                color.red("选择错误" + $const.Enter);
                $const.scanner.nextLine();
            }
        }
    }

    public static void uninstall(String packages) throws InterruptedException, IOException {
        $const.cmd("cls");
        if (packages == null) {
            color.blue_ln("请输入包名: ");
            packages = $const.scanner.nextLine();
        }
        String uninstall = $const.cmd_out($const.adb_exe + " shell pm uninstall " + packages).replace("\n", "");
        if (uninstall.equals("Success")) {
            color.green("卸载成功" + $const.Enter);
            $const.scanner.nextLine();
            return;
        }
        color.red("ERR: 卸载失败, 错误信息: " + uninstall + $const.Enter);
        $const.scanner.nextLine();
    }

    public static void disable(String packages) throws InterruptedException, IOException {
        $const.cmd("cls");
        if (packages == null) {
            color.blue_ln("请输入包名: ");
            packages = $const.scanner.nextLine();
        }
        String disable = $const.cmd_out($const.adb_exe + " shell pm disable-user " + packages).replace("\n", "");
        if (disable.equals("Package " + packages + " new state: disabled-user")) {
            color.green("冻结成功" + $const.Enter);
            $const.scanner.nextLine();
            return;
        }
        color.red("ERR: 冻结失败 输出: " + disable + $const.Enter);
        $const.scanner.nextLine();
    }

    public static void enable(String packages) throws InterruptedException, IOException {
        $const.cmd("cls");
        if (packages == null) {
            color.blue_ln("请输入包名: ");
            packages = $const.scanner.nextLine();
        }
        String enable = $const.cmd_out($const.adb_exe + " shell pm enable " + packages).replace("\n", "");
        if (enable.equals("Package " + packages + " new state: enabled")) {
            color.green("解冻成功" + $const.Enter);
            $const.scanner.nextLine();
            return;
        }
        color.red("ERR: 解冻失败 输出: " + enable + $const.Enter);
        $const.scanner.nextLine();
    }
}
