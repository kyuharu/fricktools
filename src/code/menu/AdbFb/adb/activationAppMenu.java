package code.menu.AdbFb.adb;

import java.io.IOException;

import code.tool.menu.menu;
import code.$const;
import code.tool.color;
import code.menu.home.homeMenu;

public class activationAppMenu {
    public static void start() throws InterruptedException, IOException{
        while (true) {
            menu sub=new menu();
            sub.setMenu_name("激活APP菜单(ADB)");
            sub.setTips("输入 999 返回");
            String[] home_sele = {"Shizuku","冰箱(IceBox)","黑阀(Brevent)"};
            sub.add_all(home_sele);
            sub.print();
            color.blue_ln($const.UserMeow);
            String UserMeow=$const.scanner.nextLine();
            if (UserMeow.equals("999")) {
                homeMenu.start();
                return;
            }
            if (!menu.CheckMeow(UserMeow, home_sele.length+1, 1)) {
                color.red_bold($const.UserMeow_ERR);
                code.$const.scanner.nextLine();
                start();
                return;
            }
            sele(UserMeow);
        }
    }
    
    private static void sele(String UserMeow) throws InterruptedException, IOException{
        int $UserMeow = Integer.parseInt(UserMeow);
        switch ($UserMeow) {
            case 1:
                $const.cmd($const.adb_exe+" shell sh /storage/emulated/0/Android/data/moe.shizuku.privileged.api/start.sh");
                color.green("执行成功"+$const.Enter);
                $const.scanner.nextLine();
                break;
            case 2:
                $const.cmd($const.adb_exe+" shell sh /sdcard/Android/data/com.catchingnow.icebox/files/start.sh");
                color.green("执行成功, IceBox部分机型无法正常激活, 建议使用官方激活器"+$const.Enter);
                $const.scanner.nextLine();
                break;
            case 3:
                $const.cmd($const.adb_exe+" shell sh /data/data/me.piebridge.brevent/brevent.sh");
                color.green("执行成功"+$const.Enter);
                $const.scanner.nextLine();
                break;
            default:
                color.red("ERR: 未知错误");
                break;
        }
    }
}