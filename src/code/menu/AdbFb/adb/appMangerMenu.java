
// 如你所见，这是一个实例，他将教会你如何使用 code.tool.menu.menu 类

package code.menu.AdbFb.adb;

import java.io.IOException;

import code.tool.menu.menu;
import code.$const;
import code.tool.color;

public class appMangerMenu {
    public static void start() throws InterruptedException, IOException {
        while (true) {
            menu sub = new menu();
            sub.setMenu_name("应用管理菜单");
            sub.setTips("输入 999 返回");
            String[] home_sele = { "应用列表",
                    "卸载应用",
                    "冻结应用",
                    "解冻应用" };
            sub.add_all(home_sele);
            sub.print();
            color.blue_ln($const.UserMeow);
            String UserMeow = $const.scanner.nextLine();
            if (UserMeow.equals("999")) {
                adbMenu.start();
                return;
            }
            if (!menu.CheckMeow(UserMeow, home_sele.length + 1, 1)) {
                color.red_bold($const.UserMeow_ERR);
                code.$const.scanner.nextLine();
                start();
                return;
            }
            sele(UserMeow);
        }
    }

    private static void sele(String UserMeow) throws InterruptedException, IOException {
        int $UserMeow = Integer.parseInt(UserMeow);
        switch ($UserMeow) {
            case 1:
                appManger.app_list_ui();
                break;
            case 2:
                appManger.uninstall(null);
                break;
            case 3:
                appManger.disable(null);
                break;
            case 4:
                appManger.enable(null);
                break;
            default:
                color.red("ERR: 未知错误");
                break;
        }
    }
}

