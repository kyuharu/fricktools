package code.menu.AdbFb.adb;

import cn.luern0313.lson.LsonUtil;
import cn.luern0313.lson.element.LsonObject;
import code.$const;
import code.menu.littleTools.littleTools;
import code.tool.menu.menu;
import code.tool.color;
import code.menu.AdbFb.device;

import java.io.*;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.List;
import java.util.regex.Pattern; /**
 * adb
 */
 public class adb {
    public static List<device> getDevices(){
        List<device> aDevices=new ArrayList<>();
        try {
            BufferedReader bReader=new BufferedReader(
                new InputStreamReader(
                    Runtime.getRuntime().exec($const.adb_exe+" devices").getInputStream()
                )
            );
            String line;
            StringBuilder sBuilder=new StringBuilder();
            while ((line = bReader.readLine()) != null) {
                sBuilder.append(line).append("\n");
            }
            int i = sBuilder.toString().indexOf($const.adb_devices_start);
            if (i==-1) {
                color.red("ERR: 未知错误"+$const.Enter);
                $const.scanner.nextLine();
                return null;
            }
            i=$const.adb_devices_start.length()+1;
            String deviceString= sBuilder.substring(i);
            String[] devices_outStrings=deviceString.split("\n");
            for (String string : devices_outStrings) {
                String[] device_out=string.split("\t");
                device foo = new device(device_out[0], device_out[1]);
                aDevices.add(foo);
            }
        } catch (Exception e) {
            color.red("ERR: 可能没有连接安卓设备"+$const.Enter);
            $const.scanner.nextLine();
        }
        return aDevices;
    }
    public static void reboot(String to) throws InterruptedException, IOException {
        $const.cmd("cls");
        if (to == null) {
            $const.cmd($const.adb_exe+" reboot");
            return;
        }
        $const.cmd($const.adb_exe+" reboot "+to);
    }
    public static void install(String path) throws InterruptedException, IOException{
        $const.cmd("cls");
        if (path == null) {
            color.blue_ln("请输入APK安装包路径(如: C:\\install.apk): ");
            path=$const.scanner.nextLine();
        }
        File apkfile=new File(path);
        if (!(apkfile.isFile())) {
            color.red("文件不存在"+$const.Enter);
            $const.scanner.nextLine();
            return;
        }
        $const.cmd($const.adb_exe+" install "+path);
        color.green("执行完成"+$const.Enter);
        $const.scanner.nextLine();
    }
    public static void srccer() throws InterruptedException, IOException{
        String date=String.valueOf(System.currentTimeMillis());
        $const.cmd("cls");
        File dirFile=new File("");
        String path=dirFile.getAbsolutePath()+"\\screebcap";
        File screenFile=new File(path);
        if (!(screenFile.isDirectory())) {
            boolean ifok=screenFile.mkdirs();
            if (!(ifok)) {
                color.red("目录创建失败"+$const.Enter);
                return;
            }
            color.green("必备目录创建成功!");
        }
        color.green_ln("截图将存放至 ");
        color.blue_ln(path);
        color.green(" 目录");
        String path_name="frit_"+date+".png";
        $const.cmd($const.adb_exe+" shell rm -r /sdcard/frick_cache");
        $const.cmd($const.adb_exe+" shell mkdir /sdcard/frick_cache");
        $const.cmd($const.adb_exe+" shell screencap -p /sdcard/frick_cache/"+path_name);
        $const.cmd($const.adb_exe+" pull /sdcard/frick_cache/"+path_name+" "+path+"\\"+path_name);
        $const.cmd($const.adb_exe+" shell rm -r /sdcard/frick_cache");
        $const.cmd("start "+path);
        color.green("执行完毕"+$const.Enter);
        $const.scanner.nextLine();
    }
    public static void connect(String ip,String port) throws InterruptedException, IOException{
        if (ip==null) {
            color.blue_ln("请输入IP: ");
            ip=$const.scanner.nextLine();
        }
        if (port==null) {
            color.blue_ln("请输入端口: ");
            port=$const.scanner.nextLine();
        }
        $const.cmd($const.adb_exe+" connect "+ip+":"+port);
        color.green("执行成功, 请自行查看是否正常连接"+$const.Enter);
        $const.scanner.nextLine();
    }
    public static void wm_size(String mode, String width, String height, String dpi) throws InterruptedException, IOException{
        $const.cmd("cls");
        switch (mode) {
            case "query" -> {
                String size = $const.cmd_out($const.adb_exe + " shell wm size");
                String _dpi = $const.cmd_out($const.adb_exe + " shell wm density");
                int wm_size_i = size.indexOf("Physical size: ");
                if (wm_size_i == -1) {
                    color.red("ERR: 未搜索到相关字符" + $const.Enter);
                    $const.scanner.nextLine();
                    return;
                }
                int wm_size_o = size.indexOf("Override size: ");
                wm_size_i = wm_size_i + 15;
                color.green_ln("默认分辨率: ");
                if (wm_size_o == -1) {
                    color.blue(size.substring(wm_size_i));
                } else {
                    color.blue(size.substring(wm_size_i, wm_size_o));
                }
                if (wm_size_o != -1) {
                    wm_size_o = wm_size_o + 15;
                    color.green_ln("当前分辨率: ");
                    color.blue(size.substring(wm_size_o));
                }
                int wm_dpi_i = _dpi.indexOf("Physical density: ");
                int wm_dpi_o = _dpi.indexOf("Override density: ");
                if (wm_dpi_i == -1) {
                    color.red("ERR: 未搜索到相关字符" + $const.Enter);
                    $const.scanner.nextLine();
                    return;
                }
                wm_dpi_i = wm_dpi_i + 18;
                color.green_ln("默认DPI: ");
                if (wm_dpi_o == -1) {
                    color.blue(_dpi.substring(wm_dpi_i));
                } else {
                    color.blue(_dpi.substring(wm_dpi_i, wm_dpi_o));
                }
                if (wm_dpi_o != -1) {
                    wm_dpi_o = wm_dpi_o + 18;
                    color.green_ln("当前DPI: ");
                    color.blue(_dpi.substring(wm_dpi_o));
                }
                color.green("查询成功" + $const.Enter);
                $const.scanner.nextLine();
            }
            case "set" -> {
                if (height == null) {
                    color.blue_ln("请输入高度(单位: 像素): ");
                    height = $const.scanner.nextLine();
                    final Pattern NUMBER_PATTERN = Pattern.compile("-?\\d+(\\.\\d+)?");
                    boolean check = height != null && NUMBER_PATTERN.matcher(height).matches();
                    if (!check) {
                        color.red("ERR: 请输入数字" + $const.Enter);
                        $const.scanner.nextLine();
                    }
                }
                if (width == null) {
                    color.blue_ln("请输入宽度(单位: 像素): ");
                    width = $const.scanner.nextLine();
                    final Pattern NUMBER_PATTERN = Pattern.compile("-?\\d+(\\.\\d+)?");
                    boolean check = width != null && NUMBER_PATTERN.matcher(width).matches();
                    if (!check) {
                        color.red("ERR: 请输入数字" + $const.Enter);
                        $const.scanner.nextLine();
                    }
                }
                if (dpi == null) {
                    color.blue_ln("请输入DPI(单位: 像素): ");
                    dpi = $const.scanner.nextLine();
                    final Pattern NUMBER_PATTERN = Pattern.compile("-?\\d+(\\.\\d+)?");
                    boolean check = dpi != null && NUMBER_PATTERN.matcher(dpi).matches();
                    if (!check) {
                        color.red("ERR: 请输入数字" + $const.Enter);
                        $const.scanner.nextLine();
                    }
                }
                $const.cmd($const.adb_exe + " shell wm size " + width + "x" + height);
                $const.cmd($const.adb_exe + " shell wm density " + dpi);
                color.green("执行成功" + $const.Enter);
                $const.scanner.nextLine();
            }
            case "reset" -> {
                $const.cmd("cls");
                $const.cmd($const.adb_exe + " shell wm size reset");
                $const.cmd($const.adb_exe + " shell wm density reset");
                color.green("执行成功" + $const.Enter);
                $const.scanner.nextLine();
            }
        }

    }
    public static void sideload(String ZipFile) throws IOException, InterruptedException {
        $const.cmd("cls");
        if (ZipFile == null) {
            color.blue_ln("请输入卡刷包文件路径: ");
            ZipFile=$const.scanner.nextLine();
        }
        File ZipFile_File=new File(ZipFile);
        if (!(ZipFile_File.isFile())){
            color.red_ln("""
                    未找到此文件, 是否继续执行?
                    (1) 是
                    (2) 否
                    请选择:""");
            String UserMeow=$const.scanner.nextLine();
            if (UserMeow.equals("2")) {
                return;
            }else if (!(UserMeow.equals("1"))) {
                color.red("未知选择"+$const.Enter);
                $const.scanner.nextLine();
                return;
            }
        }
        color.green("避免出现错误, 正在尝试杀死ADB...");
        littleTools.KillADB(false);
        $const.cmd($const.adb_exe+" sideload "+ZipFile);
        color.green("执行成功"+$const.Enter);
        $const.scanner.nextLine();
    }
    public static void scrcpy(String Mode) throws IOException, InterruptedException {
        if (Mode.equals("Menu")){
            menu ScrcpyMenu = new menu();
            ScrcpyMenu.setMenu_name("投屏控制器菜单");
            ScrcpyMenu.setTips("输入 999 返回");
            ScrcpyMenu.add_sele(1,"修改配置","green");
            ScrcpyMenu.add_sele(2,"启动投屏","green");
            ScrcpyMenu.print();
            color.blue_ln("请选择: ");
            String UserMeow=$const.scanner.nextLine();
            switch (UserMeow) {
                case "1" -> {
                    color.red("正在制作中" + $const.Enter);
                    $const.scanner.nextLine();
                    scrcpy("Menu");
                }
                case "2" -> scrcpy("Run");
                case "999" -> adbMenu.start();
                default -> {
                    color.red("未知选项" + $const.Enter);
                    $const.scanner.nextLine();
                }
            }
        } else if (Mode.equals("Run")) {
            $const.cmd("cls");
            String cmd = $const.scrcpy_exe + " ";
            String fileName = ".\\assets\\scrcpy\\scrcpy.json";
            String s = Files.readString(Paths.get(fileName));
            LsonObject scrcpy_config = LsonUtil.parseAsObject(s);
            if (scrcpy_config.getBoolean("no-audio")) {
                cmd = cmd + "--no-audio ";
            }
            if (scrcpy_config.getBoolean("no-control")) {
                cmd = cmd + "--no-control ";
            }
            if (scrcpy_config.getBoolean("forward-all-clicks")) {
                cmd = cmd + "--forward-all-clicks ";
            }
            cmd = cmd + "--push-target=" + scrcpy_config.getString("push-target") + " ";
            if (scrcpy_config.getBoolean("stay-awake")) {
                cmd = cmd + "--stay-awake ";
            }
            if (scrcpy_config.getBoolean("show-touches")) {
                cmd = cmd + "--show-touches ";
            }
            if (scrcpy_config.getBoolean("power-off-on-close")) {
                cmd = cmd + "--power-off-on-close ";
            }
            if (scrcpy_config.getBoolean("otg")) {
                cmd = cmd + "otg";
            }
            color.green("正在运行");
            $const.cmd_out(cmd);
            color.green("运行结束" + $const.Enter);
            $const.scanner.nextLine();
        }
    }
}
