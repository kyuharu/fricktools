
// 如你所见，这是一个实例，他将教会你如何使用 code.tool.menu.menu 类

package code.menu.plugin;

import cn.luern0313.lson.LsonUtil;
import cn.luern0313.lson.element.LsonArray;
import code.$const;
import code.tool.menu.menu;
import code.tool.httpGetRequest;
import code.tool.color;
import code.menu.home.homeMenu;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;

public class DownloadPlugin {

    static ArrayList<plugin> plugin_list;

    static {
        try {
            plugin_list = get_plugin_list();
        } catch (InterruptedException | IOException e) {
            throw new RuntimeException(e);
        }
    }

    public DownloadPlugin() {
    }

    //    StartClass
    public static void start() throws InterruptedException, IOException{
        while (true) {
            menu sub=new menu();
            sub.setMenu_name("插件市场");
            sub.setTips("输入 999 返回");
//            import sub

            for (int i=0; i<plugin_list.size(); i++) {
                plugin plugin=plugin_list.get(i);
                String name=plugin.getName();
                String version=plugin.getVersion();
                sub.add_sele(i+1, name+": "+version, null);
            }
            sub.print();
            color.blue_ln($const.UserMeow);
            String UserMeow=$const.scanner.nextLine();
            if (UserMeow.equals("999")) {
                pluginMenu.start();
                return;
            }
            if (!menu.CheckMeow(UserMeow, plugin_list.size()+1, 1)) {
                color.red_bold($const.UserMeow_ERR);
                $const.scanner.nextLine();
                start();
                return;
            }
            sele(UserMeow);
        }
    }

//    sealed func
    private static void sele(String UserMeow) throws InterruptedException, IOException{
        int UserPlugin = Integer.parseInt(UserMeow) - 1;

        // Download plugin
        plugin WillDownload = plugin_list.get(UserPlugin);
        String plugin_name=WillDownload.getName();
        String plugin_url=WillDownload.getUrl();

        // Tell User
        color.green("您即将下载 " + plugin_name + " 插件");

        // Check Installed
        if(new File(".\\plugin\\" + plugin_name.toLowerCase()).isDirectory()) {
            color.red(plugin_name + " 插件已安装, 请勿重复安装"+$const.Enter);
            $const.scanner.nextLine();
            start();
            return;
        }

        // Create Directory
        if (!(new File(".\\plugin\\" + plugin_name.toLowerCase()).mkdirs())){
            color.red("文件夹创建失败"+$const.Enter);
            $const.scanner.nextLine();
            start();
            return;
        }

        // Download
        $const.DownloadFile(plugin_url, ".\\plugin\\" + plugin_name.toLowerCase());

        // Unzip
        String ZipPath=$const.unzip(".\\plugin\\" + plugin_name.toLowerCase(), ".\\plugin\\" + plugin_name.toLowerCase());
        if(ZipPath==null){
            color.red("解压失败"+$const.Enter);
            $const.scanner.nextLine();
            start();
            return;
        }

        // Delete Zip
        if (!(new File(ZipPath).delete())){
            color.red("文件删除失败"+$const.Enter);
            $const.scanner.nextLine();
            start();
            return;
        }

        // Tell User
        color.green(plugin_name + " 插件安装成功"+$const.Enter);
        $const.scanner.nextLine();
        homeMenu.start();
    }

//    Get plugin_list
    private static ArrayList<plugin> get_plugin_list() throws InterruptedException, IOException{
        ArrayList<plugin> plugin_list=new ArrayList<>();
        LsonArray lson_array = LsonUtil.parseAsArray(httpGetRequest.sendGetRequest($const.plugin_list_url));
        for (int i=0; i< lson_array.size(); i++) {
            String plugin_name=lson_array.getJsonObject(i).getString("name");
            String plugin_version=lson_array.getJsonObject(i).getString("version");
            String plugin_url=lson_array.getJsonObject(i).getString("url");
            plugin_list.add(new plugin(plugin_name, plugin_version, plugin_url));
        }
        return plugin_list;
    }
}