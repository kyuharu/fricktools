
// 如你所见，这是一个实例，他将教会你如何使用 code.tool.menu.menu 类

package code.menu.plugin;

import code.$const;
import code.tool.menu.menu;
import code.tool.color;
import code.menu.home.homeMenu;

import java.io.IOException;

public class pluginMenu {
    public static void start() throws InterruptedException, IOException{
        while (true) {
            menu sub=new menu();
            sub.setMenu_name("插件管理");
            sub.setTips("仅支持安装官方提供的插件, 输入 999 返回");
            String[] home_sele = {"下载插件","更新插件"};
            sub.add_all(home_sele);
            sub.print();
            color.blue_ln($const.UserMeow);
            String UserMeow=$const.scanner.nextLine();
            if (UserMeow.equals("999")) {
                homeMenu.start();
                return;
            }
            if (!menu.CheckMeow(UserMeow, home_sele.length+1, 1)) {
                color.red_bold($const.UserMeow_ERR);
                $const.scanner.nextLine();
                start();
                return;
            }
            sele(UserMeow);
        }
    }
    
    private static void sele(String UserMeow) throws InterruptedException, IOException{
        int $UserMeow = Integer.parseInt(UserMeow);
        switch ($UserMeow) {
            case 1:
                DownloadPlugin.start();
                break;
            case 2:
                color.red("正在开发中"+$const.Enter);
                $const.scanner.nextLine();
                break;
            default:
                color.red("ERR: 未知错误");
                break;
        }
    }
}

//class plugin{
//    public static boolean check(String StartFile){
//        return new File(StartFile).isFile();
//    }
//}

