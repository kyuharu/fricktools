package code.menu.home;

import java.io.IOException;

import code.tool.menu.menu;
import code.$const;
import code.tool.color;
import code.menu.AdbFb.AdbFbMenu;
import code.menu.bootLoader.bootloaderMenu;
import code.menu.flash.flashMenu;
import code.menu.littleTools.littleToolsMenu;
import code.menu.plugin.pluginMenu;
import code.menu.setting.settingMenu;

public class homeMenu {
    public static void start() throws InterruptedException, IOException {
        menu home = new menu();
        home.setMenu_name("主菜单");
        home.setTips("输入 999 退出");
        String[] home_sele = { "ADB 及 FastBoot菜单", "通用刷机菜单", "小工具菜单","BL锁解锁菜单","插件管理菜单","设置" };
        home.add_all(home_sele);
        home.print();
        color.blue_ln($const.UserMeow);
        String UserMeow = $const.scanner.nextLine();
        if (UserMeow.equals("999")) {
            System.exit(0);
        }
        if (!menu.CheckMeow(UserMeow, home_sele.length + 1, 1)) {
            color.red_bold($const.UserMeow_ERR);
            code.$const.scanner.nextLine();
            start();
            return;
        }
        sele(UserMeow);
    }

    private static void sele(String UserMeow) throws InterruptedException, IOException {
        int $UserMeow = Integer.parseInt(UserMeow);
        switch ($UserMeow) {
            case 1:
                AdbFbMenu.start();
                return;
            case 2:
                flashMenu.start();
                return;
            case 3:
                littleToolsMenu.start();
                return;
            case 4:
                bootloaderMenu.start();
                return;
	        case 5:
		        pluginMenu.start();
		        return;
            case 6:
                settingMenu.start();
                return;
            default:
                color.red("ERR: 未知错误");
                break;
        }
    }
}