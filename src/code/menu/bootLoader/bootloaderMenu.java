package code.menu.bootLoader;

import code.$const;
import code.menu.home.homeMenu;
import code.tool.color;
import code.tool.menu.menu;

import java.io.IOException;

public class bootloaderMenu {
    public static void start() throws InterruptedException, IOException {
        while (true) {
            menu sub = new menu();
            sub.setMenu_name("BL锁解锁菜单");
            sub.setTips("输入 999 返回");
            String[] home_sele = {"老机型 FastBoot_oem 解锁", "MTK-Client 强解", "紫光展锐通用方案(老机型)"};
            sub.add_all(home_sele);
            sub.print();
            color.blue_ln($const.UserMeow);
            String UserMeow = $const.scanner.nextLine();
            if (UserMeow.equals("999")) {
                homeMenu.start();
                return;
            }
            if (!menu.CheckMeow(UserMeow, home_sele.length + 1, 1)) {
                color.red_bold($const.UserMeow_ERR);
                $const.scanner.nextLine();
                start();
                return;
            }
            sele(UserMeow);
        }
    }

    private static void sele(String UserMeow) throws InterruptedException, IOException {
        int $UserMeow = Integer.parseInt(UserMeow);
        switch ($UserMeow) {
            case 1:
                bootloader.oem();
                break;
            case 2:
                bootloader.mtk();
                break;
            case 3:
                bootloader.ZGTY("Menu");
                break;
            default:
                color.red("ERR: 未知错误");
                break;
        }
    }
}
