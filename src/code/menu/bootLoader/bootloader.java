
// 如你所见，这是一个实例，他将教会你如何使用 code.tool.menu.menu 类

package code.menu.bootLoader;

import java.io.File;
import java.io.IOException;
import java.util.Objects;

import code.tool.menu.menu;
import code.$const;
import code.tool.color;
import code.menu.AdbFb.adb.adb;

public class bootloader{
    public static void oem() throws IOException, InterruptedException {
        $const.cmd("cls");
        color.blue_ln("设备是否已重启至FastBoot?(是/否): ");
        String UserMeow=$const.scanner.nextLine();
        if(UserMeow.equals("否")){
            color.green("正在尝试重启至FastBoot");
            adb.reboot("bootloader");
        }else if (!(UserMeow.equals("是"))){
            color.red("输入错误"+$const.Enter);
            $const.scanner.nextLine();
            return;
        }
        $const.cmd($const.fastboot_exe+" oem unlock");
        $const.cmd($const.fastboot_exe+" flashing unlock");
        color.green("执行完成, 此方法过于老旧极有可能不成功"+$const.Enter);
        $const.scanner.nextLine();
    }
    public static void mtk() throws IOException, InterruptedException {
        $const.cmd("cls");
        color.green("此模块使用了 https://github.com/bkerler/mtkclient 开源项目");
        File mtk_py=new File($const.mtk);
        File py=new File($const.py_exe);
        if (!(mtk_py.isFile()) || !(py.isFile())){
            color.red("请确保已安装 Python 和 MtkClient 插件"+$const.Enter);
            $const.scanner.nextLine();
            return;
        }
        color.blue_ln("此操作会清除手机全部数据, 是否执行?\n(1) 是\n(2) 否\n请选择: ");
        String UserMeow = $const.scanner.nextLine();
        if (UserMeow.equals("2")){
            return;
        }else if (!(UserMeow.equals("1"))){
            color.red("错误选择"+$const.Enter);
            $const.scanner.nextLine();
            return;
        }
        $const.cmd($const.py_exe_mtk+" e metadata,userdata,md_udc");
        $const.cmd($const.py_exe_mtk+" da seccfg unlock");
        $const.cmd($const.py_exe_mtk+" reset");
        color.green("执行成功"+$const.Enter);
        $const.scanner.nextLine();
    }
    public static void ZGTY(String Mode) throws IOException, InterruptedException {
        switch (Mode) {
            case "Menu" -> {
                menu zgty_menu = new menu();
                zgty_menu.setMenu_name("紫光展锐通用解锁方案");
                zgty_menu.setTips("输入 999 返回\n> 先进行步骤一, 后进行步骤二");
                String[] zgty_sele = {"步骤一", "步骤二"};
                zgty_menu.add_all(zgty_sele);
                zgty_menu.print();
                color.blue_ln($const.UserMeow);
                String UserMeow = $const.scanner.nextLine();
                switch (UserMeow) {
                    case "999" -> bootloaderMenu.start();
                    case "1" -> ZGTY("first");
                    case "2" -> ZGTY("second");
                    default -> {
                        color.red("输入错误" + $const.Enter);
                        $const.scanner.nextLine();
                    }
                }
            }
            case "first" -> {
                $const.cmd("cls");
                color.green("您正在执行步骤一");
                color.blue("请确保手机已正常开机并开启了开发者模式, 且启动了 USB 调试" + $const.cEnter);
                $const.scanner.nextLine();
                color.green("正在检测设备连接...");
                if (Objects.requireNonNull(adb.getDevices()).isEmpty()) {
                    color.red("设备未正常连接" + $const.Enter);
                    $const.scanner.nextLine();
                    return;
                }
                color.green("设备已连接" + $const.cEnter);
                $const.scanner.nextLine();
                color.green("正在获取设备序列号...");
                String serial = $const.cmd_out($const.adb_exe + " shell getprop ro.serialno");
                color.green("设备序列号: " + serial);
                color.green("正在尝试安装 SpreadTrumTools ...");
                $const.cmd($const.adb_exe + " install .\\assets\\aft\\zg\\STT.apk");
                color.green("安装完成");
                color.green("尝试打开 STT ...");
                $const.cmd_out($const.adb_exe + " shell am start -n com.wuxianlin.spreadtrumtools/com.wuxianlin.spreadtrumtools.MainActivity");
                color.green("请将界面中输入框中的 unknown 替换为 114514 并点击 GENERATE 按钮" + $const.cEnter);
                $const.scanner.nextLine();
                color.green("正在传输签名文件...");
                $const.cmd_out($const.adb_exe + " pull /sdcard/signature.bin .\\assets\\aft\\zg\\signature.bin");
                color.green("传输完成");
                color.green("步骤一结束" + $const.Enter);
                $const.scanner.nextLine();
            }
            case "second" -> {
                $const.cmd("cls");
                color.green("您正在执行步骤二");
                color.blue("请确保手机已正常开机并开启了开发者模式, 且启动了 USB 调试" + $const.cEnter);
                $const.scanner.nextLine();
                color.green("检测是否拥有签名文件...");
                if (!(new File(".\\assets\\aft\\zg\\signature.bin").isFile())) {
                    color.red("签名文件不存在, 请先执行步骤一" + $const.Enter);
                    $const.scanner.nextLine();
                    return;
                }
                color.green("签名文件存在");
                color.green("正在重启至 FastBoot ...");
                adb.reboot("bootloader");
                color.green("全自动执行命令");
                $const.cmd($const.zg_fastboot_exe + " oem get_identifier_token");
                $const.cmd($const.zg_fastboot_exe + " flashing unlock_bootloader .\\assets\\aft\\zg\\signature.bin");
                color.green("步骤二结束, 请根据屏幕提示, 按音量下键解锁" + $const.Enter);
                $const.scanner.nextLine();
            }
        }
    }
}