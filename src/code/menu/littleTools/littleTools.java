package code.menu.littleTools;

import code.$const;
import code.tool.color;
import code.tool.processManger;

import java.io.File;
import java.io.IOException;

public class littleTools {
    public static void startShell() throws InterruptedException, IOException {
        $const.cmd("cls");
        File file = new File("");
        $const.cmd("start powershell.exe cmd /k 'set path=" + file.getAbsolutePath() + "\\assets\\aft" + "'");
        color.green("打开成功" + $const.Enter);
        $const.scanner.nextLine();
    }

    public static void tar(String bao, String to) throws InterruptedException, IOException {
        if (bao == null) {
            color.blue_ln("请输入线刷包路径(如: D:\\miui-xxx-xxx.tar): ");
            bao = $const.scanner.nextLine();
        }
        if (to == null) {
            color.blue_ln("请输入解压路径(文件夹, 如: D:\\mymiui): ");
            to = $const.scanner.nextLine();
        }
        File baoFile = new File(bao);
        if (!(baoFile.isFile())) {
            color.red("ERR: 无法找到此线刷包" + $const.Enter);
            $const.scanner.nextLine();
            return;
        }
        $const.cmd($const.tar_exe + " -xzvf " + bao + " -C " + to);
        $const.cmd("start " + to);
        color.green("执行成功" + $const.Enter);
        $const.scanner.nextLine();
    }

    public static void payload(String path, String subarea) throws InterruptedException, IOException {
        $const.cmd("cls");
        color.green_bold("本模块使用 Android-ota-payload-extractor(https://github.com/tobyxdd/android-ota-payload-extractor) 开源项目二次开发");
        File payFile = new File(".\\payload");
        if (!(payFile.isDirectory())) {
            if (payFile.mkdirs()) {
                color.green("建立必备文件夹成功");
            } else {
                color.red("未能建立必备文件夹" + $const.Enter);
                $const.scanner.nextLine();
                return;
            }
        }
        if (path == null) {
            color.blue_ln("请输入 Payload.bin 文件路径(如: C:\\payload.bin): ");
            path = $const.scanner.nextLine();
            File paFile = new File(path);
            if (!(paFile.isFile())) {
                color.red("ERR: 没有此文件" + $const.Enter);
                $const.scanner.nextLine();
                return;
            }
        }
        if (subarea == null) {
            color.blue_ln("请输入要提取的 分区(如: boot、vendor等): ");
            subarea = $const.scanner.nextLine();
            if (subarea.isEmpty()) {
                color.red("ERR: 请选择" + $const.Enter);
                $const.scanner.nextLine();
                return;
            }
            if (subarea.contains("\t")) {
                color.red("ERR: 请勿输入空格" + $const.Enter);
                $const.scanner.nextLine();
            }
        }
        $const.cmd($const.payload_exe + " " + path + " " + subarea);
        File imgFile = new File(".\\" + subarea + ".img");
        if (!(imgFile.isFile())) {
            color.red("提取失败" + $const.Enter);
            $const.scanner.nextLine();
            return;
        }
        File nimgFile = new File(".\\payload\\" + subarea + ".img");
        if (nimgFile.isFile()) {
            if (!nimgFile.delete()){
                color.red("删除失败"+$const.Enter);
            }
        }
        if (!(imgFile.renameTo(nimgFile))) {
            color.red("移动失败" + $const.Enter);
            $const.scanner.nextLine();
            return;
        }
        $const.cmd("start .\\payload");
        color.green("提取完成" + $const.Enter);
        $const.scanner.nextLine();
    }

    public static void KillADB(boolean TZ) throws IOException {
        processManger.killProcess("adb.exe");
        if (TZ) {
            color.green("已尝试杀死" + $const.Enter);
            $const.scanner.nextLine();
        }
    }
    public static void Aria2Download(int thread, String DownloadDir, String Url, boolean TZ) throws IOException, InterruptedException {
        $const.cmd("cls");
        if (thread == 0){
            color.blue_ln("请输入线程数(默认: 16): ");
            thread = Integer.parseInt($const.scanner.nextLine());
        }
        if (DownloadDir == null) {
            color.blue_ln("请输入保存路径(文件夹): ");
            DownloadDir = $const.scanner.nextLine();
            if (!(new File(DownloadDir).isDirectory())) {
                color.red("ERR: 路径不存在" + $const.Enter);
                $const.scanner.nextLine();
                return;
            }
        }
        if (Url == null) {
            color.blue_ln("请输入链接: ");
            Url = $const.scanner.nextLine();
            if (Url.isEmpty()) {
                color.red("ERR: 请输入链接" + $const.Enter);
                $const.scanner.nextLine();
                return;
            }
        }
        String cmd = $const.aria2c_exe + " -d " + DownloadDir + " -x " + thread + " -s " + thread + " " + Url;
        $const.cmd(cmd);
        if (TZ){
            color.green("下载完成" + $const.Enter);
            $const.scanner.nextLine();
        }
    }

    public static void pan123FastDownload() throws IOException, InterruptedException {
        $const.cmd("cls");

        // check file
        if (!new File($const.pan123).isFile()) {
            color.red("ERR: 文件丢失, 请重新安装"+$const.Enter);
            $const.scanner.nextLine();
            return;
        }

        $const.cmd("cd .\\assets\\pan123 && start powershell.exe cmd /k '.\\android.exe'");
        color.green("启动成功, 自动打开下载目录"+$const.Enter);

        if (!new File(".\\assets\\pan123\\download").isDirectory()) {
            if (!new File(".\\assets\\pan123\\download").mkdirs()){
                color.red("ERR: 文件夹创建失败");
                return;
            }
        }

        $const.cmd("start .\\assets\\pan123\\download");
        $const.scanner.nextLine();
    }
}
