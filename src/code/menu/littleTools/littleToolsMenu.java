package code.menu.littleTools;

import java.io.IOException;

import code.tool.menu.menu;
import code.$const;
import code.tool.color;
import code.menu.home.homeMenu;

public class littleToolsMenu {
    public static void start() throws InterruptedException, IOException {
        while (true) {
            menu sub = new menu();
            sub.setMenu_name("小工具菜单");
            sub.setTips("输入 999 返回");
            String[] home_sele = { "打开一个拥有 adb 和 fastboot 环境的终端",
                    "解压线刷包",
                    "提取 Payload.bin",
                    "杀死所有 ADB 进程",
                    "多线程下载器",
                    "123 云盘无视限流下载"};
            sub.add_all(home_sele);
            sub.print();
            color.blue_ln($const.UserMeow);
            String UserMeow = $const.scanner.nextLine();
            if (UserMeow.equals("999")) {
                homeMenu.start();
                return;
            }
            if (!menu.CheckMeow(UserMeow, home_sele.length + 1, 1)) {
                color.red_bold($const.UserMeow_ERR);
                code.$const.scanner.nextLine();
                start();
                return;
            }
            sele(UserMeow);
        }
    }

    private static void sele(String UserMeow) throws InterruptedException, IOException {
        int $UserMeow = Integer.parseInt(UserMeow);
        switch ($UserMeow) {
            case 1:
                littleTools.startShell();
                break;
            case 2:
                littleTools.tar(null, null);
                break;
            case 3:
                littleTools.payload(null, null);
                break;
            case 4:
                littleTools.KillADB(true);
                break;
            case 5:
                littleTools.Aria2Download(0,null,null,true);
                break;
            case 6:
                littleTools.pan123FastDownload();
                break;
            default:
                color.red("ERR: 未知错误");
                break;
        }
    }
}

