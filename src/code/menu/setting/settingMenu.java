
// 如你所见，这是一个实例，他将教会你如何使用 code.tool.menu.menu 类

package code.menu.setting;

import code.$const;
import code.tool.menu.menu;
import code.tool.color;
import code.menu.home.homeMenu;

import java.io.IOException;

public class settingMenu {
    public static void start() throws InterruptedException, IOException{
        while (true) {
            menu sub=new menu();
            sub.setMenu_name("设置菜单");
            sub.setTips("输入 999 返回");
            String[] home_sele = {"关于", "开源"};
            sub.add_all(home_sele);
            sub.print();
            color.blue_ln($const.UserMeow);
            String UserMeow=$const.scanner.nextLine();
            if (UserMeow.equals("999")) {
                homeMenu.start();
                return;
            }
            if (!menu.CheckMeow(UserMeow, home_sele.length+1, 1)) {
                color.red_bold($const.UserMeow_ERR);
                $const.scanner.nextLine();
                start();
                return;
            }
            sele(UserMeow);
        }
    }
    
    private static void sele(String UserMeow) throws InterruptedException, IOException{
        int $UserMeow = Integer.parseInt(UserMeow);
        switch ($UserMeow) {
            case 1:
                $const.cmd("cls");
                color.red_ln("1.6 Gamma ");
                color.blue("Full");
                color.green_ln("补丁版本: ");
                color.blue("1.6.2 Alpha");
                color.green_ln("作者: ");
                color.blue("Kyuharu");
                color.green_ln("Gitee: ");
                color.blue("https://gitee.com/kyuharu/fricktools");
                color.green_ln("爱发电: ");
                color.blue("https://afdian.com/a/kyuharu");
                color.green("按 Enter 返回...");
                $const.scanner.nextLine();
                settingMenu.start();
                return;
            case 2:
                licenseMenu.start();
                break;
            default:
                color.red("ERR: 未知错误");
                break;
        }
    }
}