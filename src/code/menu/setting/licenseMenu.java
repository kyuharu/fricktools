
// 如你所见，这是一个实例，他将教会你如何使用 code.tool.menu.menu 类

package code.menu.setting;

import code.$const;
import code.tool.menu.menu;
import code.tool.color;

import java.io.IOException;

public class licenseMenu {
    public static void start() throws InterruptedException, IOException{
        while (true) {
            menu sub=new menu();
            sub.setMenu_name("开源协议");
            sub.setTips("输入 999 返回");
            String[] home_sele = {"自身", "Android-ota-payload-extractor", "MtkClient", "Lson", "Scrcpy"};
            sub.add_all(home_sele);
            sub.print();
            color.blue_ln($const.UserMeow);
            String UserMeow=$const.scanner.nextLine();
            if (UserMeow.equals("999")) {
                settingMenu.start();
                return;
            }
            if (!menu.CheckMeow(UserMeow, home_sele.length+1, 1)) {
                color.red_bold($const.UserMeow_ERR);
                $const.scanner.nextLine();
                start();
                return;
            }
            sele(UserMeow);
        }
    }
    
    private static void sele(String UserMeow) throws InterruptedException, IOException{
        int $UserMeow = Integer.parseInt(UserMeow);
        switch ($UserMeow) {
            case 1:
                $const.cmd("start .\\assets\\License\\FrickTools.txt");
                break;
            case 2:
                $const.cmd("start .\\assets\\License\\Android-ota-payload-extractor.txt");
                break;
            case 3:
                $const.cmd("start .\\assets\\License\\MtkClient.txt");
                break;
            case 4:
                $const.cmd("start .\\assets\\License\\Lson.txt");
                break;
            case 5:
                $const.cmd("start .\\assets\\License\\Scrcpy.txt");
                break;
            default:
                color.red("ERR: 未知错误");
                break;
        }
    }
}