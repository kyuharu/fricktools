import code.$const;
import code.menu.home.homeMenu;
import code.tool.color;

import java.io.IOException;
import java.net.InetAddress;
import java.net.Socket;

public class App{
    public static void main(String[] args) throws Exception {
        new ProcessBuilder("cmd","/c","cls").inheritIO().start().waitFor();
        color.green_bold("Frit Starting");
        color.green("查找可用端口中...");
        int port = getUsablePort(5037);
        $const.cmd_out($const.adb_exe+" -P "+port+" start-server");
        color.green_ln("Adb已在可用端口 ");
        color.blue_ln(Integer.toString(port));
        color.green(" 上启动");
        Runtime.getRuntime().addShutdownHook(new ShutDown());
        color.red("------------");
        color.green_bold("特别鸣谢: ");
        color.blue("竹、简单、忘乎世界、全幼儿园最帅、天格洛情");
        color.green("感谢你们参与 BETA 测试并提供宝贵意见！");
        color.red("------------");
        color.green_bold("免责声明：");
        color.green(code.$const.FreeLiability);
        color.green_bold("按Enter同意并进入, 否则请勿使用本工具...");
        code.$const.scanner.nextLine();
        homeMenu.start();
    }

    public static int getUsablePort(int port) throws IOException{
        boolean flag = false;
        InetAddress theAddress = InetAddress.getByName("127.0.0.1");
        try (Socket ignored1 = new Socket(theAddress, port)) {
            flag = true;
        } catch (IOException ignored) {
        }

        if (flag){
            port = port+1;
            return getUsablePort(port);
        } else {
            return port;
        }
    }
}