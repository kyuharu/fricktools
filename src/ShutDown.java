import code.$const;

import java.io.IOException;

public class ShutDown extends Thread{
    @Override
    public void run() {
        try {
            $const.cmd($const.adb_exe+" kill-server");
        } catch (InterruptedException | IOException e) {
            throw new RuntimeException(e);
        }
    }
}
