<div align="center"><img alt="FRIT Logo" src="https://afrit.cn/img/logo-no-background.svg" width="200"/></div>
<h1 align="center">搞姬工具箱</h1>

<div align="center">
搞姬工具箱是一款开源、免费的安卓刷机、玩机、搞机工具箱
</div>

------------

# 🆔 版本描述
此版本为基础Windows版本，除安卓版本外，其余版本全部基于本版开发

------------

# 💖 下载地址
- FastRIT 云盘：[https://p.afrit.cn/s/3NuK](https://p.afrit.cn/s/3NuK "https://p.afrit.cn/s/3NuK")
- Gitee Releases：[https://gitee.com/kyuharu/fricktools/releases/](https://gitee.com/kyuharu/fricktools/releases/ "https://gitee.com/kyuharu/fricktools/releases/")

------------

*tips: 如有**下载站、网盘**等想要收录本项目欢迎联系kyuharu@foxmail.com邮箱~*

------------

# 🔨 开发环境
- Java: JDK-17

------------

# 🎉 特别鸣谢
竹、简单、忘乎世界、全幼儿园最帅、天格枫菜
感谢你们参与BETA测试并提供宝贵意见！

------------

# ✅ 实现功能
- ADB 及 FastBoot 菜单
  - 检测设备
  - ADB 菜单
    - 重启
    - 重启至FastBoot
    - 重启至Recovery
    - 重启至Download(挖煤模式)
    - 安装APK
    - Shell终端
    - 截图
    - 无线调试
    - 激活APP菜单(ADB)
      - Shizuku
      - 冰箱（Ice Box）
      - 黑阀(Brevent)
    - 修改分辨率
    - 应用管理菜单
      - 应用列表
        - 当前系统安装的应用
        - 未冻结的应用
        - 已冻结的应用
      - 卸载应用
      - 冻结应用
      - 解冻应用
    - SideLoad 旁加载
    - 投屏控制器
      - 修改配置(正在制作)
      - 启动投屏
  - FastBoot菜单
    - 重启至系统
    - 重启至Recovery
    - 重启至EDL(9008,仅高通系列)
    - 擦除分区
    - 查看BL锁状态(部分机型不支持)
    - 临时启动BOOT
    - 刷入指定文件夹的全部镜像
- 一键刷入类
  - 刷入Recovery
  - 刷入Boot
  - 刷入任意分区
  - 一键刷入线刷包
- 小工具菜单
  - 打开一个具备ADB环境的终端窗口
  - 解压线刷包
  - 提取Payload
  - 杀死所有 ADB 进程
  - 多线程下载器
- Bl锁解锁菜单
  - 老机型 FastBoot-oem 解锁
  - Mtk-Client 强解
  - 紫光展锐通用方案
    - 步骤一
    - 步骤二
- 插件菜单
  - 下载插件
  - 更新插件
- 设置
  - 关于
  - 开源

# 🔶 大家投稿的功能
*tips: 这个分类是指投稿但还没打算去实现的功能*

------------

- 未分类
  - 精简系统软件

# 💌 投稿
由于作者没什么想法，所以更新的动力一部分来源于大家的投稿，以下是投稿方式：
1. 爱发电私聊(回复速度：慢)
2. 哔哩哔哩私聊(回复速度：一般)
3. 发邮件(邮箱：kyuharu@foxmail.com 回复速度：快)

# 🔗 作者的各种链接
- 哔哩哔哩主页：[https://space.bilibili.com/2075226503/](https://space.bilibili.com/2075226503/ "https://space.bilibili.com/2075226503/")
- 爱发电主页：[https://afdian.net/a/kyuharu/](https://afdian.net/a/kyuharu/ "https://afdian.net/a/kyuharu/")
- 邮箱地址：kyuharu@foxmail.com
- 官网：[http://fricktools.fun/](http://fricktools.fun/ "http://fricktools.fun/")

# 🧡 赞助
*tips: 作者更新软件很累的，赞助一下吧！（未成年切勿赞助，你的心意我心领了！）*
- 爱发电：[https://afdian.net/a/kyuharu/](https://afdian.net/a/kyuharu/ "https://afdian.net/a/kyuharu/")

# ✨ 使用到的开源项目
- Android-ota-payload-extractor：[https://github.com/tobyxdd/android-ota-payload-extractor](https://github.com/tobyxdd/android-ota-payload-extractor "https://github.com/tobyxdd/android-ota-payload-extractor")
- MtkClient：[https://github.com/bkerler/mtkclient](https://github.com/bkerler/mtkclient "https://github.com/bkerler/mtkclient")
- Lson：[https://github.com/luern0313/Lson](https://github.com/luern0313/Lson "https://github.com/luern0313/Lson")
- Scrcpy: [https://github.com/Genymobile/scrcpy](https://github.com/Genymobile/scrcpy "https://github.com/Genymobile/scrcpy")